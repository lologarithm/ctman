package ctm

import "time"

type UserSettings struct {
	Name          string
	FriendlyName  string
	DailySettings []DaySetting
}

type DaySetting struct {
	Limit    int32 // seconds of time
	Mod      int32 // seconds extra time for the day
	UnlockAt int32 // seconds after midnight computer unlocks
	LockAt   int32 // seconds after midnight computer locks
}

type Consumed struct {
	Weekday     time.Weekday
	Consumed    int32 // Absolute Seconds Consumed. Used for GET and direct value on PUT
	ConsumedMod int32 // Relative Seconds Consumed... Used for PUT to add time used.
}
