package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/lologarithm/ctman/ctm"
)

func main() {
	var username = flag.String("user", "", "user to manage")
	var masterPass = flag.String("pass", "", "system master password")
	var timeServer = flag.String("server", "http://192.168.1.50:3335", "time server url")
	flag.Parse()

	envpass := os.Getenv("PARENTPASS")

	if *masterPass == "" {
		fmt.Printf("Master passunset, env pass is set...\n")
		masterPass = &envpass
	}
	if *username == "" || *masterPass == "" {
		fmt.Printf("Username or master password not set.\n")
		os.Exit(1)
	}

	rand.Seed(time.Now().UnixNano())
	fmt.Printf("Managing user: %s\n", *username)
	StartServer(*masterPass, *username, *timeServer)

	err := http.ListenAndServe(":3334", nil)
	if err != nil {
		fmt.Printf("[ERROR] HTTP server failed: %s\n", err)
	}
}

// TODO:
//  1. track app usage
//  2. un/block apps
//  3.

type server struct {
	mu         *sync.RWMutex
	masterPass string
	username   string
	remaining  int32
	unlockTime time.Duration
	lockTime   time.Duration
	timeServer string
	client     *http.Client

	UserStats      UserStats
	checkInterrupt chan struct{}
}

func StartServer(masterPass, username, timerServer string) {
	fmt.Printf("Server Starting for user (%s)\n", username)

	s := &server{
		mu:             &sync.RWMutex{},
		masterPass:     masterPass,
		username:       username,
		timeServer:     timerServer,
		lockTime:       time.Hour * 21,      // 21 hours after midnight computer locks
		unlockTime:     time.Hour * 9,       // 9 hours after midnight computer unlocks
		checkInterrupt: make(chan struct{}), // blocking
	}

	client := &http.Client{}
	client.Timeout = time.Second * 3

	s.client = client

	// blocking before we fully start up the server.
	s.UpdateConsumed()

	go s.checkLoop()               // monitor user and lock when needed.
	s.checkInterrupt <- struct{}{} // trigger check now

	http.HandleFunc("/login", s.HandleLogin)
	http.HandleFunc("/lock", s.HandleLock)
	http.HandleFunc("/kill", s.HandleKill)       // ?pid=X
	http.HandleFunc("/killall", s.HandleKillAll) // ?pid=X
	http.HandleFunc("/", s.HandleLock)
}

// UpdateConsumed will query the timeserver and update local unlockedtime.
func (s *server) UpdateConsumed() {
	consResp, err := s.client.Get(s.timeServer + "/consumed?user=" + s.username)
	if err != nil {
		fmt.Printf("[ERROR] Time server request failed: %s\n", err)
		return
	}
	cons := &ctm.Consumed{}
	respBody, _ := ioutil.ReadAll(consResp.Body)
	json.Unmarshal(respBody, cons)

	s.mu.Lock()
	defer s.mu.Unlock()

	s.UserStats.ActiveTime = time.Duration(cons.Consumed) * time.Second
	s.UserStats.UnlockedTime = s.UserStats.ActiveTime
}

func (s *server) HandleLogin(w http.ResponseWriter, r *http.Request) {
	s.checkInterrupt <- struct{}{}
	w.WriteHeader(http.StatusOK)
}

func (s *server) HandleKill(w http.ResponseWriter, r *http.Request) {
	cmd := exec.Command("kill", r.FormValue("pid"))
	cmd.Run()
	http.Redirect(w, r, "/lock", http.StatusSeeOther)
}

func (s *server) HandleKillAll(w http.ResponseWriter, r *http.Request) {
	cmd := exec.Command("killall", r.FormValue("pid"))
	cmd.Run()
	http.Redirect(w, r, "/lock", http.StatusSeeOther)
}

var site = `<html><body>
<h2>Lock Interface</h2><br /> 
<b>Remaining Minutes Today:</b> %d <br />
<b>Today's Stats:</b> Unlocked For: %d minutes, Active Use for: %d minutes</br />
<b>Current State:</b> %s <br />
<form action="/lock" method="POST"><input type="submit" value="LOCK"></input><input type="hidden" value="-1" id="dur" name="dur"></form>
<form action="/lock" method="POST"><input type="submit" value="UNLOCK"></input><input type="text" value="10" id="dur" name="dur"><input type="password" autocomplete="false" value="" id="pass" name="pass"></form>
<p>Running Applications</p>
<pre style="width: 400">%s</pre>
</body></html>`

func (s *server) HandleLock(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("[INFO] HandleLock called from %s\n", r.RemoteAddr)
	durVal := r.FormValue("dur")
	pass := r.FormValue("pass")

	writePage := func() {
		s.mu.RLock()
		w.Write([]byte(fmt.Sprintf(site,
			s.remaining/60,
			s.UserStats.UnlockedTime/time.Minute,
			s.UserStats.ActiveTime/time.Minute,
			s.UserStats.CurrentStatus.String(),
			processList(s.username),
		)))
		s.mu.RUnlock()
	}

	dur, err := strconv.Atoi(durVal) // minutes
	if durVal == "" || err != nil {
		writePage()
		fmt.Printf("[INFO] HandleLock completed - no duration to process.\n")
		return
	}

	if dur > 0 && pass != s.masterPass {
		writePage()
		fmt.Printf("[INFO] Trying to set time but had incorrect password...\n")
		return
	}

	s.checkInterrupt <- struct{}{} // trigger check now

	fmt.Printf("[INFO] HandleLock completed.\n")
	// writePage()
	http.Redirect(w, r, "/lock", http.StatusSeeOther)
}

func (s *server) GetTimerSettings() (*ctm.UserSettings, error) {
	resp, err := s.client.Get(s.timeServer + "/settings?user=" + s.username)
	if err != nil {
		// TODO: what do we do when we can't reach settings server?
		// nmcli device

		return nil, err
	}
	settingData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	settings := &ctm.UserSettings{}
	err = json.Unmarshal(settingData, settings)
	if err != nil {
		return settings, err
	}

	return settings, nil
}

// checkLoop will loop forever watching if the user is logged in.
//   - If the user is logged in, this loop will update the timer server with the latest usage amount.
//   - If the user is out of time and logged in they will be logged out.
//   - If user is using computer outside of allowed hours, they will be logged out.
//   - If the user has gone more than 5 minutes over the computer will shutdown.
func (s *server) checkLoop() {
	go func() {
		for {
			time.Sleep(time.Minute * 5)
			s.UpdateConsumed() // just to make sure local doesnt fall out of date with server.
		}
	}()

	lastNotify := time.Time{}
	lastNotifyID := 0
	lastCheck := time.Now()
	timerDur := time.Second * 15
	doubleSleep := false
	for {
		sleepDur := timerDur
		if doubleSleep {
			sleepDur *= 2
		}
		timer := time.NewTimer(sleepDur)
		select { // select until one of them goes
		case <-timer.C:
		case <-s.checkInterrupt:
		}
		info := getUserStatus(s.username)
		s.UserStats.CurrentStatus = info.CurrentStatus

		dur := time.Since(lastCheck)
		if dur < 0 {
			fmt.Printf("[ERROR] Less than expected amount of time passed.\n")
			dur = time.Second * 30
		}
		if lastCheck.Day() != time.Now().Day() {
			s.mu.Lock()
			s.UserStats = UserStats{}
			s.mu.Unlock()
		}

		lastCheck = time.Now()
		settings, err := s.GetTimerSettings()
		if err != nil {
			fmt.Printf("[INFO] Failed to get timer settings: %s\n", err)
			// Cant get settings, then just lock computer for now.
			lock(s.username, "[INFO] Locking computer because unable to fetch computer time settings.")
			s.mu.Lock()
			s.remaining = 0
			s.mu.Unlock()
		}

		s.mu.Lock()
		if info.CurrentStatus == UserStatusActive {
			s.UserStats.ActiveTime += dur
			s.UserStats.UnlockedTime += dur
			secConsumed := int(dur / time.Second)
			if secConsumed > 0 {
				updateData := []byte(`{"ConsumedMod": ` + strconv.Itoa(secConsumed) + `}`)
				postR, err := http.Post(s.timeServer+"/consumed?user="+s.username+"&pwd="+s.masterPass, "application/json", bytes.NewReader(updateData))
				if err != nil {
					respBody, _ := ioutil.ReadAll(postR.Body)
					fmt.Printf("[ERROR] Failed to update consumed time: %#v\n", string(respBody))
					s.remaining = 0
				} else if postR.StatusCode != 200 {
					fmt.Printf("[ERROR] Got back failure code during consumed update: %#v\n", postR)
				}
			}
		} else if info.CurrentStatus == UserStatusInactive {
			s.UserStats.UnlockedTime += dur
		} else if info.CurrentStatus == UserStatusLocked {
			doubleSleep = true // we can check less frequently while computer is locked
			s.mu.Unlock()
			continue
		} else {
			fmt.Printf("[WARN] User status not parsed: %s\n", info.Raw)
		}

		// validate user is allowed to be on during this time.
		{
			today := settings.DailySettings[time.Now().Weekday()]
			if today.LockAt != 0 && today.UnlockAt != 0 {
				s.unlockTime = time.Second * time.Duration(today.UnlockAt)
				s.lockTime = time.Second * time.Duration(today.LockAt)
			}

			now := time.Now()
			dayStart := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
			sinceMidnight := time.Since(dayStart)
			if sinceMidnight < s.unlockTime || sinceMidnight > s.lockTime {
				sendNotification("NOT ALLOWED", "Not allowed to be playing right now!", 0)
				lock(s.username, fmt.Sprintf("[INFO] Locking computer because outside of approved hours (%s - %s) (%s, %s)", s.unlockTime, s.lockTime, sinceMidnight, dayStart)) // lock down user!
			}
		}

		if s.remaining < -300 {
			// more than 5 minutes past... lets just shutdown the computer
			fmt.Printf("[ERROR] Still getting time used even after attempting to lock session. Status:\n----------\n%s\n----------\n", info.Raw)
			err := exec.Command("shutdown", "now").Run()
			if err != nil {
				fmt.Printf("[ERROR] Failed to shutdown computer: %s\n", err.Error())
			}
		} else if s.remaining <= 0 {
			// if out of time, lock sessions
			if info.CurrentStatus == UserStatusActive {
				fmt.Printf("[INFO] User out of time and still active. Locking.\n")
			}
			lock(s.username, "")
			if s.remaining < -60 {
				sendNotification("NOT ALLOWED", "Stop playing when you are out of time or computer will shutdown!!", 0)
			}
		} else {
			lastNotify, lastNotifyID = notifyUserTime(s.username, lastNotify, time.Duration(s.remaining)*time.Second, lastNotifyID)
		}
		s.mu.Unlock()
	}
}

// notifyUserTime will send notification to the user of remaining time.
func notifyUserTime(username string, lastNotify time.Time, left time.Duration, notifyID int) (time.Time, int) {
	// Check until next timeout check
	if time.Since(lastNotify) > time.Minute*15 || (left < time.Minute*5 && time.Since(lastNotify) > time.Minute) {
		fmt.Printf("[INFO] Sending notification to user, %d minutes remaining\n", int(left.Minutes()))

		whocmd, err := exec.Command("who").CombinedOutput()
		if err != nil {
			fmt.Printf("Failed to execute who: %s\n", err)
		}

		for _, userline := range strings.Split(string(whocmd), "\n") {
			parts := strings.Fields(userline)
			if len(parts) < 3 {
				continue
			}
			if parts[0] != username {
				continue
			}
			output := sendNotification("Computer Time", fmt.Sprintf(" %d Minutes Remaining", left/time.Minute), notifyID)

			outputs := strings.Split(string(output), ",")
			if len(outputs) > 0 {
				values := strings.Split(outputs[0], " ")
				if len(values) > 1 {
					notifyID, _ = strconv.Atoi(values[1])
				}
			}
		}
		lastNotify = time.Now()
	}
	return lastNotify, notifyID
}

// sendNotification is just a wrapper around gdbus sending a notification message to org.freedesktop.Notifications
func sendNotification(title string, notice string, noticeID int) []byte {

	notifyCmd := exec.Command("gdbus", "call", "--session", "--dest=org.freedesktop.Notifications", "--object-path=/org/freedesktop/Notifications", "--method=org.freedesktop.Notifications.Notify",
		``, strconv.Itoa(noticeID), ``, title, notice, `[]`, `{"urgency": <1>}`, strconv.Itoa(5000))

	output, err := notifyCmd.CombinedOutput()
	if err != nil {
		fmt.Printf("[INFO] User notification failed: %s\n", err)
		if len(output) > 0 {
			fmt.Printf("Command Output:\n%s\n", string(output))
		}
	}

	return output
}

type UserStats struct {
	Date          time.Time // time these stats started on
	ActiveTime    time.Duration
	UnlockedTime  time.Duration
	CurrentStatus UserStatus // unknown, active, inactive, locked

	Raw string // raw output from status
}

type UserStatus int

const (
	UserStatusUnknown UserStatus = iota
	UserStatusActive
	UserStatusInactive
	UserStatusLocked
)

func (us UserStatus) String() string {
	switch us {
	case 1:
		return "Active Use"
	case 2:
		return "Unlocked but Inactive"
	case 3:
		return "Locked"
	}
	return "Unknown"
}

// getUserStatus will parse the output of loginctl
//   - first calls `loginctl list-sessions` to find if the user is listed
//   - if so, calls `loginctl show-session` to get details (if the user is active or not)
func getUserStatus(username string) UserStats {
	stats := UserStats{}

	out, err := exec.Command("loginctl", "list-sessions").CombinedOutput()
	if err != nil {
		fmt.Printf("[ERROR] Failed to get user stats: %s, %s \n", err.Error(), string(out))
		return stats
	}
	outLines := strings.Split(string(out), "\n")
	if len(outLines) < 1 {
		fmt.Printf("[ERROR] Failed to get user stats, too few lines returned: %s \n", string(out))
		return stats
	}

	for _, line := range outLines[1:] {
		parts := strings.Fields(line)
		if len(parts) < 5 {
			continue
		}
		if parts[2] != username {
			continue
		}
		if parts[3] == "" {
			continue
		}
		sessDetails, err := exec.Command("loginctl", "show-session", parts[0]).CombinedOutput()
		if err != nil {
			fmt.Printf("failed to execute loginctl show-sesion (%s): %s\n", err.Error(), string(sessDetails))
			os.Exit(1)
		}
		newState := UserStatusUnknown
		stats.Raw += string(sessDetails) + "\n"
	stateLoop:
		for _, line := range strings.Split(string(sessDetails), "\n") {
			kv := strings.Split(line, "=")
			if len(kv) != 2 {
				continue
			}
			switch kv[0] {
			case "State":
				if kv[1] == "active" {
					if newState == UserStatusUnknown {
						newState = UserStatusActive // dont override an inactive result from another line.
					}
				} else {
					newState = UserStatusInactive
				}
			case "Active":
				if kv[1] == "yes" {
					if newState == UserStatusUnknown {
						newState = UserStatusActive // dont override an inactive result from another line.
					}
				} else {
					newState = UserStatusInactive
				}
			case "IdleHint":
				if kv[1] == "yes" {
					newState = UserStatusInactive
				}
			case "LockedHint":
				if kv[1] == "yes" {
					newState = UserStatusLocked
					break stateLoop // locked overrides all, exit switch and for loop
				}
			}
		}
		if stats.CurrentStatus != UserStatusActive { // if another session is active, nothing overrides.
			stats.CurrentStatus = newState
		}
	}
	return stats
}

func processList(user string) string {
	output, err := exec.Command("top", "-bn", "1", "-w", "512", "-u", user).CombinedOutput()
	if err != nil {
		fmt.Printf("Failed to execute process list: %s\n%s\n", err, string(output))
		return ""
	}
	outLines := strings.Split(string(output), "\n")
	processes := []string{}

	for _, line := range outLines[7:] {
		lineFields := strings.Fields(line)
		if len(lineFields) < 12 {
			continue
		}
		pcpu, _ := strconv.ParseFloat(lineFields[8], 64)
		if pcpu < 2.0 {
			continue
		}
		name := strings.Join(lineFields[11:], " ")
		button := `<form style="display:inline" action="/kill" method="POST"><input type="submit" value="Kill"></input><input type="hidden" value="%s" id="pid" name="pid"></form>`
		processes = append(processes, strings.Split(lineFields[10], ":")[0]+"m\t"+name+"\t"+fmt.Sprintf(button, lineFields[0]))
	}

	return strings.Join(processes, "\n")
}

func lock(username string, msg string) {
	out, err := exec.Command("loginctl", "list-sessions").CombinedOutput()
	if err != nil {
		fmt.Printf("[ERROR] Failed to get user stats: %s, %s\n", err.Error(), string(out))
		return
	}
	outLines := strings.Split(string(out), "\n")
	if len(outLines) < 1 {
		fmt.Printf("[ERROR] Failed to get user stats, too few lines returned: %s\n", string(out))
		return
	}
	for _, line := range outLines[1:] {
		parts := strings.Fields(line)
		if len(parts) < 5 {
			continue
		}
		if parts[2] != username {
			continue
		}
		if msg != "" {
			fmt.Printf("%s - Locking session %s\n", msg, parts[0])
		}
		lockCmd := exec.Command("loginctl", "lock-session", parts[0])
		out, err := lockCmd.CombinedOutput()
		if err != nil {
			fmt.Printf("[INFO] Lock Session Command Failed: %s\n%s\n", err, string(out))
		} else {
			fmt.Printf("[INFO] Locked Session Successfully: %s\n", string(out))
		}
	}
}
