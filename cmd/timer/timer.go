package main

import (
	_ "embed"
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"gitlab.com/lologarithm/ctman/ctm"
	bolt "go.etcd.io/bbolt"
)

//go:embed index.html
var index []byte

func main() {
	envPass := os.Getenv("PARENTPASS")
	masterPass := ""
	flag.StringVar(&masterPass, "pass", "", "master password for managing computers")
	flag.Parse()
	if masterPass == "" {
		log.Printf("Master pass unset on command line, using env pass...")
		masterPass = envPass
	}

	db, err := bolt.Open("timer.db", 0666, nil)
	if err != nil {
		log.Fatalf("failed to open boltdb: %#v", err)
	}
	defer db.Close()

	StartServer(masterPass, db)
}

// 1. set daily limit per person
// 2. update daily modifier
// 3. fetch time remaining for the day
// 4. update time used

type server struct {
	mu  *sync.RWMutex
	db  *bolt.DB
	pwd string

	histMux *sync.RWMutex
	history map[string]UserHistory
}

type UserHistory struct {
	Hourly      [24 * 7]int64
	LastUpdated time.Time
}

func StartServer(masterPass string, db *bolt.DB) {
	s := &server{
		mu:      &sync.RWMutex{},
		db:      db,
		pwd:     masterPass,
		histMux: &sync.RWMutex{},
		history: map[string]UserHistory{},
	}
	historyjson, err := os.ReadFile("history.json")
	if err == nil {
		unmerr := json.Unmarshal(historyjson, &s.history)
		if unmerr != nil {
			oldHist := map[string][24 * 7]int64{}
			if newErr := json.Unmarshal(historyjson, &oldHist); newErr != nil {
				fmt.Printf("Failed to load history.json: %s", err)
			} else {
				for k := range oldHist {
					s.history[k] = UserHistory{
						Hourly: oldHist[k],
					}
				}
			}
		}
	}

	http.HandleFunc("/interface", s.HandleInterface)
	http.HandleFunc("/", s.HandleInterface)
	http.HandleFunc("/settings", s.HandleSettings)
	http.HandleFunc("/consumed", s.HandleUsed)
	http.HandleFunc("/users", s.HandleUsers)
	http.HandleFunc("/history", s.HandleHistory)

	go func() {
		last := time.Now()
		for {
			time.Sleep(time.Minute)
			if last.Day() != time.Now().Day() {
				s.nextDay(last.Weekday())
			}
			last = time.Now()
		}
	}()
	if err := http.ListenAndServe(":3335", nil); err != nil {
		log.Printf("[ERROR] HTTP server failed: %s", err)
	}
}

func (s *server) HandleHistory(w http.ResponseWriter, r *http.Request) {
	s.histMux.RLock()
	history, _ := json.Marshal(s.history)
	s.histMux.RUnlock()

	w.Write(history)
}

var zero = []byte{0, 0, 0, 0}

func (s *server) nextDay(prevDay time.Weekday) {
	// Reset mods for yesterday to 0. reset consumed to 0
	log.Printf("[INFO] Resetting daily data for %s.", prevDay.String())
	s.db.Update(func(tx *bolt.Tx) error {
		tx.ForEach(func(name []byte, b *bolt.Bucket) error {
			// clear out last weeks data for the new day
			nameStr := string(name)
			day := int(time.Now().Weekday()) * 24
			s.histMux.Lock()
			userHistory := s.history[nameStr]
			for i := 0; i < 24; i++ {
				userHistory.Hourly[day+i] = 0
			}
			s.history[nameStr] = userHistory
			s.histMux.Unlock()

			err := b.Put(consumedKey, zero)
			if err != nil {
				log.Printf("[ERROR] Failed to write reset of daily data.")
				return err
			}

			v := b.Get(settingsKey)
			setts := &ctm.UserSettings{}
			json.Unmarshal(v, setts)

			setts.DailySettings[prevDay].Mod = 0 // reset modified bonus from the previous day.
			newV, _ := json.Marshal(setts)
			err = b.Put(settingsKey, newV)
			return err
		})
		return nil
	})
}

// serves a UI
func (s *server) HandleInterface(w http.ResponseWriter, r *http.Request) {
	w.Write(index)
}

func (s *server) HandleUsers(w http.ResponseWriter, r *http.Request) {
	type username struct {
		Name         string
		FriendlyName string
	}
	names := []username{}
	s.db.View(func(tx *bolt.Tx) error {
		tx.ForEach(func(name []byte, b *bolt.Bucket) error {
			v := b.Get(settingsKey)
			un := username{}
			json.Unmarshal(v, &un)
			names = append(names, un)
			return nil
		})
		return nil
	})
	data, err := json.Marshal(names)
	if err != nil {
		log.Printf("[ERROR] Failed to marshal users JSON struct: %#v", err)
	}
	w.Write(data)
}

var settingsKey = []byte("settings")
var consumedKey = []byte("consumed")
var lastUsedKey = []byte("lastUsed")

// GET: fetch
// POST: update
func (s *server) HandleSettings(w http.ResponseWriter, r *http.Request) {
	// log.Printf("[INFO] Serving /settings %s", r.Method)
	user := r.URL.Query().Get("user")
	if len(user) == 0 || user == "undefined" {
		return
	}
	bucketKey := []byte(user)
	switch r.Method {
	case "GET":
		s.db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket(bucketKey)
			if b == nil {
				w.Write([]byte(`{"error": "no such user"}`))
				return nil
			}
			v := b.Get(settingsKey)
			w.Write(v)
			return nil
		})
	case "POST":
		if r.URL.Query().Get("pwd") != s.pwd {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		body, err := io.ReadAll(r.Body)
		if err != nil {
			log.Printf("Error reading request body: %#v", err)
		}
		s.db.Update(func(tx *bolt.Tx) error {
			b, err := tx.CreateBucketIfNotExists(bucketKey)
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}

			err = b.Put(settingsKey, body)
			return err
		})
	}
}

// GET: {'Consumed': 30}
// POST: {'Consumed': 30} OR {'ConsumedMod': 1}
func (s *server) HandleUsed(w http.ResponseWriter, r *http.Request) {
	user := r.URL.Query().Get("user")
	if len(user) == 0 || user == "undefined" {
		return
	}
	bucketKey := []byte(user)
	v := []byte{}
	switch r.Method {
	case "GET":
		s.db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket(bucketKey)
			if b == nil {
				w.Write([]byte(`{'error': "user does not exist"}`))
				return nil
			}
			v = b.Get(consumedKey)
			return nil
		})

		cons := &ctm.Consumed{
			Weekday: time.Now().Weekday(),
		}
		if len(v) == 4 {
			cons.Consumed = int32(binary.LittleEndian.Uint32(v))
		}
		out, _ := json.Marshal(cons)
		w.Header().Set("Content-Type", "application/json")
		w.Write(out)
	case "POST":
		if r.URL.Query().Get("pwd") != s.pwd {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		body, err := io.ReadAll(r.Body)
		if err != nil {
			log.Printf("Error reading request body: %#v", err)
		}
		consumeUpdate := &ctm.Consumed{}
		err = json.Unmarshal(body, consumeUpdate)
		if err != nil {
			log.Printf("[WARN] Failed to unmarshal consume update message: %#v", err)
			w.Write([]byte(`{"error": "failed to read input"}`))
			return
		}
		err = s.db.Update(func(tx *bolt.Tx) error {
			b, err := tx.CreateBucketIfNotExists(bucketKey)
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}
			newV := consumeUpdate.ConsumedMod
			if newV != 0 { // if this is a mod value, add to existing.
				v := b.Get(consumedKey)
				if len(v) == 4 {
					newV += int32(binary.LittleEndian.Uint32(v))
				}
			} else {
				newV = consumeUpdate.Consumed
			}

			// Update Consumed
			store := make([]byte, 4)
			binary.LittleEndian.PutUint32(store, uint32(newV))
			err = b.Put(consumedKey, store)

			// Update 'last updated'
			lastUpdated := make([]byte, 8)
			lastUsed := time.Now()
			binary.LittleEndian.PutUint64(lastUpdated, uint64(lastUsed.Unix()))
			b.Put(lastUsedKey, lastUpdated)

			if consumeUpdate.ConsumedMod != 0 {
				now := time.Now()
				day := now.Weekday()
				hour := now.Hour()
				bucketID := int(day)*24 + hour
				s.histMux.Lock()
				userHistory := s.history[user]
				userHistory.Hourly[bucketID] += int64(consumeUpdate.ConsumedMod)
				userHistory.LastUpdated = lastUsed
				s.history[user] = userHistory
				data, _ := json.Marshal(s.history)
				os.WriteFile("history.json", data, 0666)
				s.histMux.Unlock()
			}

			return err
		})
		if err != nil {
			log.Printf("Failed to write consume update: %s", err.Error())
		}
	}
}
