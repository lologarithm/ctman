module gitlab.com/lologarithm/ctman

go 1.16

require (
	github.com/godbus/dbus/v5 v5.0.6
	go.etcd.io/bbolt v1.3.6
)
